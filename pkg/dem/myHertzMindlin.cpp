// 2010 © Chiara Modenese <c.modenese@gmail.com>

#include "myHertzMindlin.hpp"//the first complie forget to change here
#include <lib/high-precision/Constants.hpp>
#include <core/Omega.hpp>
#include <core/Scene.hpp>
#include <pkg/dem/ScGeom.hpp>

namespace yade { // Cannot have #include directive inside.

YADE_PLUGIN((myFrictMat)(myMindlinPhys)(Ip2_myFrictMat_myFrictMat_myMindlinPhys)(
        Law2_ScGeom_myMindlinPhys_myMindlin));

Real Law2_ScGeom_myMindlinPhys_myMindlin::getfrictionDissipation() const { return (Real)frictionDissipation; }
Real Law2_ScGeom_myMindlinPhys_myMindlin::getshearEnergy() const { return (Real)shearEnergy; }
Real Law2_ScGeom_myMindlinPhys_myMindlin::getnormDampDissip() const { return (Real)normDampDissip; }
Real Law2_ScGeom_myMindlinPhys_myMindlin::getshearDampDissip() const { return (Real)shearDampDissip; }

/******************** Ip2_myFrictMat_myFrictMat_myMindlinPhys *******/
CREATE_LOGGER(Ip2_myFrictMat_myFrictMat_myMindlinPhys);

void Ip2_myFrictMat_myFrictMat_myMindlinPhys::go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction)
{
	if (interaction->phys) return; // no updates of an already existing contact necessary
	shared_ptr<myMindlinPhys> contactPhysics(new myMindlinPhys());
	interaction->phys = contactPhysics;
	const auto mat1   = YADE_CAST<myFrictMat*>(b1.get());
	const auto mat2   = YADE_CAST<myFrictMat*>(b2.get());

	/* from interaction physics */
	const Real Ea = mat1->young;
	const Real Eb = mat2->young;
	const Real Va = mat1->poisson;
	const Real Vb = mat2->poisson;
	const Real fa = mat1->frictionAngle;
	const Real fb = mat2->frictionAngle;
	const Real a_ = mat1->alpha;
	const Real l_ = mat1->lambda_r;//all from material 1?
	const Real p_ = mat1->pow_c;//all from material 1?
	//const Real alphaKra = mat1->alphaKr;
	//const Real alphaKrb = mat2->alphaKr;
	Real etaRoll = (mat2->etaRoll - mat1->etaRoll > 0) ? mat1->etaRoll : mat2->etaRoll;

	/* for record the initial */
	if (setStiffReduction && ShearDis_DefinitionIteration == -1) ShearDis_DefinitionIteration = scene->iter;
	if (setStiffReduction && ShearDis_DefinitionIteration != -1 && ShearDis_DefinitionIteration != scene->iter) {
		ShearDis_DefinitionIteration = -1;
		setStiffReduction              = 0;
	}
	if (setStiffReduction) {
		contactPhysics->initReduction = true;
	}

	/* from interaction geometry */
	const auto scg = YADE_CAST<GenericSpheresContact*>(interaction->geom.get());
	const Real Da  = scg->refR1 > 0 ? scg->refR1 : scg->refR2;
	const Real Db  = scg->refR2 > 0 ? scg->refR2 : scg->refR1;
	//Vector3r normal=scg->normal;        //The variable set but not used


	/* calculate stiffness coefficients */
	const Real Ga = Ea / (2 * (1 + Va));
	const Real Gb = Eb / (2 * (1 + Vb));
	const Real G  = 1.0 / ((2 - Va) / Ga + (2 - Vb) / Gb); // effective shear modulus
	//	const Real V             = (Va + Vb) / 2;                                                           // average of poisson's ratio
	const Real E             = Ea * Eb / ((1. - math::pow(Va, 2)) * Eb + (1. - math::pow(Vb, 2)) * Ea); // effective Young modulus
	const Real R             = Da * Db / (Da + Db);                                                     // equivalent radius
	//const Real Rmean         = (Da + Db) / 2.;                                                          // mean radius
	const Real Kno           = 4. / 3. * E * sqrt(R);                                                   // coefficient for normal stiffness
	const Real Kso           = 8 * sqrt(R) * G;                                                         // coefficient for shear stiffness
	const Real frictionAngle = (!frictAngle) ? math::min(fa, fb) : (*frictAngle)(mat1->id, mat2->id, mat1->frictionAngle, mat2->frictionAngle);

	const Real Adhesion = 4. * Mathr::PI * R * gamma; // calculate adhesion force as predicted by DMT theory

	// add some new rolling parameter, it should be noted that since a negative initial value is used, so may be we need to write as the COhesiveFrictionalContactLaw.cpp
	Real AlphaKr ;
	if (mat1->alphaKr && mat1->alphaKr) AlphaKr = 2.0 * mat1->alphaKr * mat2->alphaKr / (mat1->alphaKr + mat2->alphaKr);
	else
		AlphaKr = 0;
	//const Real AlphaKr = alphaKra*alphaKrb / ( alphaKra + alphaKrb );
	const Real Kro = Kso * R * R * AlphaKr;
	/* pass values calculated from above to myMindlinPhys */
	contactPhysics->tangensOfFrictionAngle = math::tan(frictionAngle);
	//contactPhysics->prevNormal = scg->normal; // used to compute relative rotation
	contactPhysics->kno           = Kno; // this is just a coeff
	contactPhysics->kso           = Kso; // this is just a coeff
	contactPhysics->adhesionForce = Adhesion;

	//contactPhysics->kr        = krot;
	/* *************************** */
	/* HERE is the new code startd */
	//contactPhysics->kro       = 0.1 * Kso * R * R;
	contactPhysics->kro        = Kro;
	contactPhysics->kr        = krot;
	contactPhysics->ktw       = ktwist;
	//contactPhysics->maxBendPl = eta * Rmean; // does this make sense? why do we take Rmean?
	/* *************************** */
	/* HERE is the new code startd */
	contactPhysics->maxBendPl = etaRoll * R ; // we test R instead of Rmean
	/* HERE is the new code ed */
	/* *************************** */
	
	// small strain to large strain stiffness reduciton parameters
	contactPhysics->alpha 		= a_;
	contactPhysics->lambda_r 	= l_ ;
	contactPhysics->pow_c 	    = p_ ;
	contactPhysics->ratio_knks  = Kno/Kso;
	/* the paramete to test the stiffness reduction*/
	contactPhysics->kso_o = Kso;//actually can no update
	contactPhysics->kno_o = Kno;
	contactPhysics->kso_r = Kso;//"the tangential reduction stiffness"))
	contactPhysics->kno_r = Kno;//"the normal reduction stiffness"))


	/* compute viscous coefficients */
	if (en && betan) throw std::invalid_argument("Ip2_myFrictMat_myFrictMat_myMindlinPhys: only one of en, betan can be specified.");
	if (es && betas) throw std::invalid_argument("Ip2_myFrictMat_myFrictMat_myMindlinPhys: only one of es, betas can be specified.");

	// en or es specified
	if (en || es) {
		const Real h1  = -6.918798; // Fitting coefficients h_i from  Table 2 - Thornton et al. (2013).
		const Real h2  = -16.41105;
		const Real h3  = 146.8049;
		const Real h4  = -796.4559;
		const Real h5  = 2928.711;
		const Real h6  = -7206.864;
		const Real h7  = 11494.29;
		const Real h8  = -11342.18;
		const Real h9  = 6276.757;
		const Real h10 = -1489.915;

		// Consider same coefficient of restitution if only one is given (en or es)
		if (!en) { en = es; }
		if (!es) { es = en; }

		const Real En     = (*en)(mat1->id, mat2->id);
		const Real Es     = (*es)(mat1->id, mat2->id);
		const Real alphan = En
		        * (h1
		           + En * (h2 + En * (h3 + En * (h4 + En * (h5 + En * (h6 + En * (h7 + En * (h8 + En * (h9 + En * h10))))))))); // Eq. (B7) from Thornton et al. (2013)
		contactPhysics->betan = (En == 1.0) ? 0
		                                    : sqrt(1.0 / (1.0 - (math::pow(1.0 + En, 2)) * exp(alphan))
		                                           - 1.0); // Eq. (B6) from Thornton et al. (2013) - This is noted as 'gamma' in their paper

		// although Thornton (2015) considered betan=betas, here we use his formulae (B6) and (B7) allowing for betas to take a different value, based on the input es
		const Real alphas     = Es * (h1 + Es * (h2 + Es * (h3 + Es * (h4 + Es * (h5 + Es * (h6 + Es * (h7 + Es * (h8 + Es * (h9 + Es * h10)))))))));
		contactPhysics->betas = (Es == 1.0) ? 0 : sqrt(1.0 / (1.0 - (math::pow(1.0 + Es, 2)) * exp(alphas)) - 1.0);

		// betan/betas specified, use that value directly
	} else {
		contactPhysics->betan = betan ? (*betan)(mat1->id, mat2->id) : 0;
		contactPhysics->betas = betas ? (*betas)(mat1->id, mat2->id) : contactPhysics->betan;
	}
}

/* Function to count the number of adhesive contacts in the simulation at each time step */
Real Law2_ScGeom_myMindlinPhys_myMindlin::contactsAdhesive() // It is returning something rather than zero only if includeAdhesion is set to true
{
	Real contactsAdhesive = 0;
	FOREACH(const shared_ptr<Interaction>& I, *scene->interactions)
	{
		if (!I->isReal()) continue;
		myMindlinPhys* phys = dynamic_cast<myMindlinPhys*>(I->phys.get());
		if (phys->isAdhesive) { contactsAdhesive += 1; }
	}
	return contactsAdhesive;
}

/* Function which returns the ratio between the number of sliding contacts to the total number at a given time */
Real Law2_ScGeom_myMindlinPhys_myMindlin::ratioSlidingContacts()
{
	Real ratio(0);
	int  count(0);
	FOREACH(const shared_ptr<Interaction>& I, *scene->interactions)
	{
		if (!I->isReal()) continue;
		myMindlinPhys* phys = dynamic_cast<myMindlinPhys*>(I->phys.get());
		if (phys->isSliding) { ratio += 1; }
		count++;
	}
	ratio /= count;
	return ratio;
}

/* Function to get the NORMAL elastic potential energy of the system */
Real Law2_ScGeom_myMindlinPhys_myMindlin::normElastEnergy()
{
	Real normEnergy = 0;
	FOREACH(const shared_ptr<Interaction>& I, *scene->interactions)
	{
		if (!I->isReal()) continue;
		ScGeom*      scg  = dynamic_cast<ScGeom*>(I->geom.get());
		myMindlinPhys* phys = dynamic_cast<myMindlinPhys*>(I->phys.get());
		if (phys) {
			if (includeAdhesion) {
				normEnergy += (math::pow(scg->penetrationDepth, 5. / 2.) * 2. / 5. * phys->kno - phys->adhesionForce * scg->penetrationDepth);
			} else {
				normEnergy += math::pow(scg->penetrationDepth, 5. / 2.) * 2. / 5. * phys->kno;
			} // work done in the normal direction. NOTE: this is the integral
		}
	}
	return normEnergy;
}

/* Function to get the adhesion energy of the system */
Real Law2_ScGeom_myMindlinPhys_myMindlin::adhesionEnergy()
{
	Real adhesionEnergy = 0;
	FOREACH(const shared_ptr<Interaction>& I, *scene->interactions)
	{
		if (!I->isReal()) continue;
		ScGeom*      scg  = dynamic_cast<ScGeom*>(I->geom.get());
		myMindlinPhys* phys = dynamic_cast<myMindlinPhys*>(I->phys.get());
		if (phys && includeAdhesion) {
			Real R       = scg->radius1 * scg->radius2 / (scg->radius1 + scg->radius2);
			Real gammapi = phys->adhesionForce / (4. * R);
			adhesionEnergy += gammapi * pow(phys->radius, 2);
		} // note that contact radius is calculated if we calculate energy components
	}
	return adhesionEnergy;
}


/******************** Law2_ScGeom_myMindlinPhys_myMindlin *********/
CREATE_LOGGER(Law2_ScGeom_myMindlinPhys_myMindlin);

bool Law2_ScGeom_myMindlinPhys_myMindlin::go(shared_ptr<IGeom>& ig, shared_ptr<IPhys>& ip, Interaction* contact)
{
	const Real& dt = scene->dt; // get time step

	Body::id_t id1 = contact->getId1(); // get id body 1
	Body::id_t id2 = contact->getId2(); // get id body 2

	auto* de1 = Body::byId(id1, scene)->state.get();
	auto* de2 = Body::byId(id2, scene)->state.get();

	ScGeom*      scg  = static_cast<ScGeom*>(ig.get());
	myMindlinPhys* phys = static_cast<myMindlinPhys*>(ip.get());

	const shared_ptr<Body>& b1 = Body::byId(id1, scene);
	const shared_ptr<Body>& b2 = Body::byId(id2, scene);

	bool useDamping = (phys->betan != 0. || phys->betas != 0.);

#ifdef PARTIALSAT
	if (contact->isFresh(scene)) {
		phys->initD = scg->penetrationDepth; // only useful for partialsat break criteria
	}
#endif
	// tangential and normal stiffness coefficients, recomputed from betan,betas at every step
	Real cn = 0, cs = 0;

	/****************/
	/* NORMAL FORCE */
	/****************/

	Real uN = scg->penetrationDepth; // get overlapping
	if (uN < 0) {
		if (neverErase) {
			phys->shearForce = phys->normalForce = Vector3r::Zero();
			phys->kn = phys->ks = 0;
			return true;
		} else
			return false;
	}

	/*******************************/
	/* TANGENTIAL NORMAL STIFFNESS */
	/*******************************/

	phys->kn = 3. / 2. * phys->kno * math::pow(uN, 0.5); // here we store the value of kn to compute the time step

	/******************************/
	/* TANGENTIAL SHEAR STIFFNESS */
	/******************************/

	phys->ks = phys->kso * math::pow(uN, 0.5); // get tangential stiffness (this is a tangent value, so we can pass it to the GSTS)
	
    /* *************************** */
	/* HERE is the new code startd */
	//Real R_ = scg->radius1 * scg->radius2 / (scg->radius1 + scg->radius2);
	//phys->kr = 0.10 * phys->ks * R_ * R_; //also need to update 
	if (includeReduction && isHyperbolic) {
		//Real ks0 = phys->kso * math::pow(uN, 0.5);//initial ks
		Real a_ = phys->alpha;
		Real l_ = phys->lambda_r;
		Real p_ = phys->pow_c;
		Real s  = phys->accumS;
		Real R_ = scg->radius1 * scg->radius2 / (scg->radius1 + scg->radius2);
		////Real s0  = phys->accumS0;
		//// the following is not const !
		//Real R_ = scg->radius1 * scg->radius2 / (scg->radius1 + scg->radius2);
		Real tmp = (s )/(R_*l_);
		//phys->tmp = tmp;
		//Real reduction1 = a_ + (1.0-a_)/(1.0 +  pow(tmp, 4.0));
		Real reduction = a_ + (1.0-a_)/(1.0 + pow(tmp,p_));
		 
		//phys->kso_o = phys->kso * math::pow(uN, 0.5);// actually can no update
		//phys->kno_o = 1.5 * phys->ks_o * phys->ratio_knks;
		phys->kso_r = phys->kso_o * reduction;//"the tangential reduction stiffness"))
		phys->kno_r = phys->kno_o * reduction;//"the normal reduction stiffness"))
		
		// donot reduction first, see whether only this botton the result is still the same
		if ( shearReduction ) {
			phys->kso = phys->kso_r  ;
			phys->kro = phys->kro * (phys->kso_r/phys->kso_o); }
		if ( normalReduction ) { phys->kno = phys->kno_r  ;}
		
	}
	/* anothre solution similar to the boundary surface*/
	if (includeReduction && !isHyperbolic) {
		Real normalF = phys->normalForce.norm();
	 
		Real maxShearF = normalF * phys->tangensOfFrictionAngle;
		Real currFs = phys->shearForce.norm();
		
		Real rho;
		Real phi;
		if (currFs==0.0) {
			rho = 1.e10;
			phi = 1.e10;
		} else {
			rho = maxShearF/currFs;
			phi = (maxShearF-currFs)/currFs ;
		}// error boost::math::constants::phi
		
		phys->rho = rho;
		phys->phi = phi;
		 
		Real p_ = phys->pow_c;
		Real a_ = phys->alpha;
		Real E = phys->kso_o;
		Real b_ = a_/(1.0 - a_);
		Real K = b_ * E * math::pow(rho,p_) * exp(  phi * p_ ) ; //
		//Real K = b_ * E * exp(  phi * p_ );
		//Real K = b_ * E * math::pow(rho,p_) ;

		Real reduction = E * E / (E + K); //since we use kso instead of ks, kso would no be 0
		//Real reduction;
		//if (E==0.0) {
		//	reduction = 0.0;
		//} else {
		//	
		//	reduction = E*E/(E + K );//E*E/(E + K);
		//}
		
		
		phys->kso_r = phys->kso_o - reduction;//"the tangential reduction stiffness"))
 		phys->kno_r = phys->kso_r * phys->ratio_knks;// HOW to reduct the kn in a more reasonable way?
		
		// donot reduction first, see whether only this botton the result is still the same
		if ( shearReduction ) {
			phys->kso = phys->kso_r  ;
			phys->kro = phys->kro * (phys->kso_r/phys->kso_o); }
		if ( normalReduction ) { phys->kno = phys->kno_r  ;}
		
	}
	/* HERE is the new code ended  */
	/* *************************** */
	
	

	/****************/
	/* NORMAL FORCE */
	/****************/
	/* Hertz-Mindlin's formulation (PFC)
	Note that the normal stiffness here is a secant value (so as it is cannot be used in the GSTS)
	In the first place we get the normal force and then we store kn to be passed to the GSTS */
	//Real Fn = phys->kno * math::pow(uN, 1.5); // normal Force (scalar)
	Real Fn = (2.0/3.0)* phys->kn * math::pow(uN, 1.0); // normal Force (scalar) using kn to calculate
	//if (includeReduction) {
	//	Fn = (2.0/3.0)* phys->kn_r * math::pow(uN, 1.0); 
	//}
	if (includeAdhesion) {
		Fn -= phys->adhesionForce;   // include adhesion force to account for the effect of Van der Waals interactions
		phys->isAdhesive = (Fn < 0); // set true the bool to count the number of adhesive contacts
	}
	phys->normalForce = Fn * scg->normal; // normal Force (vector)

	if (calcEnergy) {
		Real R = scg->radius1 * scg->radius2 / (scg->radius1 + scg->radius2);
		phys->radius
		        = pow((Fn + (includeAdhesion ? phys->adhesionForce : 0.)) * pow(R, 3 / 2.) / phys->kno,
		              1 / 3.); // attribute not used anywhere, we do not need it
	}

	
	/************************/
	/* DAMPING COEFFICIENTS */
	/************************/

	// Inclusion of local damping if requested
	// viscous damping is defined for both linear and non-linear elastic case
	if (useDamping) { // see Thornton (2015)
		Real mbar    = (!b1->isDynamic() && b2->isDynamic())
		           ? de2->mass
		           : ((!b2->isDynamic() && b1->isDynamic())
		                      ? de1->mass
		                      : (de1->mass * de2->mass
                                      / (de1->mass
                                         + de2->mass))); // get equivalent mass if both bodies are dynamic, if not set it equal to the one of the dynamic body
		Real Cn_crit = 2. * sqrt(mbar * phys->kn);  // Critical damping coefficient (normal direction)
		Real Cs_crit = 2. * sqrt(mbar * phys->ks);  // Critical damping coefficient (shear direction)

		cn = Cn_crit * phys->betan; // Damping normal coefficient
		cs = Cs_crit * phys->betas; // Damping tangential coefficient
		if (phys->kn < 0 || phys->ks < 0) {
			cerr << "Negative stiffness kn=" << phys->kn << " ks=" << phys->ks << " for ##" << b1->getId() << "+" << b2->getId() << ", step "
			     << scene->iter << endl;
		}
	}

	/***************/
	/* SHEAR FORCE */
	/***************/

	Vector3r& shearElastic = phys->shearElastic; // reference for shearElastic force
	// Define shifts to handle periodicity
	const Vector3r shift2   = scene->isPeriodic ? scene->cell->intrShiftPos(contact->cellDist) : Vector3r::Zero();
	const Vector3r shiftVel = scene->isPeriodic ? scene->cell->intrShiftVel(contact->cellDist) : Vector3r::Zero();
	// 1. Rotate shear force
	shearElastic            = scg->rotate(shearElastic);
	Vector3r prev_FsElastic = shearElastic; // save shear force at previous time step
	                                        // 2. Get incident velocity, get shear and normal components
	// NOTE: below, the normal component is obtained from getIncidentVel(), OTOH, the shear component computed at next line would be wrong for sphere-facet
	// and possibly other Ig types incompatible with preventGranularRatcheting=true. We thus use the precomputed shearIncrement from the Ig2, which should be always correct.
	Vector3r incidentV = scg->getIncidentVel(de1, de2, dt, shift2, shiftVel, false);
	//     Vector3r incidentV  = geom->shearIncrement()/dt;
	Vector3r incidentVn = scg->normal.dot(incidentV) * scg->normal; // contact normal velocity
	Vector3r incidentVs = scg->shearIncrement() / dt;               // contact shear velocity
	// 3. Get shear force (incrementally)
	//if (includeReduction) {
	//	shearElastic = shearElastic - phys->ks_r * (incidentVs * dt);
	//}
	//else {
	//	shearElastic = shearElastic - phys->ks * (incidentVs * dt);
	//}
	shearElastic = shearElastic - phys->ks * (incidentVs * dt);

	/**************************************/
	/* VISCOUS DAMPING (Normal direction) */
	/**************************************/

	// normal force must be updated here before we apply the Mohr-Coulomb criterion
	if (useDamping) { // get normal viscous component
		phys->normalViscous = cn * incidentVn;
		Vector3r normTemp   = phys->normalForce - phys->normalViscous; // temporary normal force
		// viscous force should not exceed the value of current normal force, i.e. no attraction force should be permitted if particles are non-adhesive
		// if particles are adhesive, then fixed the viscous force at maximum equal to the adhesion force
		// *** enforce normal force to zero if no adhesion is permitted ***
		if (phys->adhesionForce == 0.0 || !includeAdhesion) {
			if (normTemp.dot(scg->normal) < 0.0) {
				phys->normalForce   = Vector3r::Zero();
				phys->normalViscous = phys->normalViscous
				        + normTemp; // normal viscous force is such that the total applied force is null - it is necessary to compute energy correctly!
			} else {
				phys->normalForce -= phys->normalViscous;
			}
		} else if (includeAdhesion && phys->adhesionForce != 0.0) {
			// *** limit viscous component to the max adhesive force ***
			if (normTemp.dot(scg->normal) < 0.0 && (phys->normalViscous.norm() > phys->adhesionForce)) {
				Real     normVisc       = phys->normalViscous.norm();
				Vector3r normViscVector = phys->normalViscous / normVisc;
				phys->normalViscous     = phys->adhesionForce * normViscVector;
				phys->normalForce -= phys->normalViscous;
			}
			// *** apply viscous component - in the presence of adhesion ***
			else {
				phys->normalForce -= phys->normalViscous;
			}
		}
		if (calcEnergy) { normDampDissip += phys->normalViscous.dot(incidentVn * dt); } // calc dissipation of energy due to normal damping
	}


	/*************************************/
	/* SHEAR DISPLACEMENT (elastic only) */
	/*************************************/

	Vector3r& us_elastic = phys->usElastic;
	us_elastic           = scg->rotate(us_elastic); // rotate vector
	Vector3r prevUs_el   = us_elastic;              // store previous elastic shear displacement (already rotated)
	us_elastic -= incidentVs * dt;                  // add shear increment

	/****************************************/
	/* SHEAR DISPLACEMENT (elastic+plastic) */
	/****************************************/

	Vector3r& us_total  = phys->usTotal;
	us_total            = scg->rotate(us_total); // rotate vector
	Vector3r prevUs_tot = us_total;              // store previous total shear displacement (already rotated)
	us_total -= incidentVs
	        * dt; // add shear increment NOTE: this vector is not passed into the failure criterion, hence it holds also the plastic part of the shear displacement
	
	/* *************************** */
	/* HERE is the new code startd */
	Vector3r tmp = incidentVs * dt;// (us_total - prevUs_tot); set not used error?!
	if (includeReduction) {
		phys->accumS += tmp.norm();// += tmp.norm()
	}
	// phys->accumS0 += scg->shearIncrement().norm();// += tmp.norm()
	// may use the scg.shearInc.norm() to calculat the phys->accumS?
	/* HERE is the new code ended  */
	/* *************************** */
	
	bool noShearDamp = false; // bool to decide whether we need to account for shear damping dissipation or not

	/********************/
	/* MOHR-COULOMB law */
	/********************/
	phys->isSliding    = false;
	phys->shearViscous = Vector3r::Zero(); // reset so that during sliding, the previous values is not there
	Fn                 = phys->normalForce.norm();
	if (!includeAdhesion) {
		Real maxFs = Fn * phys->tangensOfFrictionAngle;
		if (shearElastic.squaredNorm() > maxFs * maxFs) {
			phys->isSliding = true;
			noShearDamp     = true; // no damping is added in the shear direction, hence no need to account for shear damping dissipation
			Real ratio      = maxFs / shearElastic.norm();
			shearElastic *= ratio;
			phys->shearForce = shearElastic; /*store only elastic shear displacement*/
			us_elastic *= ratio;
			if (calcEnergy) {
				frictionDissipation += (us_total - prevUs_tot).dot(shearElastic);
			}                                     // calculate energy dissipation due to sliding behavior
		} else if (useDamping) {                      // add current contact damping if we do not slide and if damping is requested
			phys->shearViscous = cs * incidentVs; // get shear viscous component
			phys->shearForce   = shearElastic - phys->shearViscous;
		} else if (!useDamping) {
			phys->shearForce = shearElastic;
		} // update the shear force at the elastic value if no damping is present and if we passed MC
	} else {  // Mohr-Coulomb formulation adpated due to the presence of adhesion (see Thornton, 1991).
		Real maxFs = phys->tangensOfFrictionAngle * (phys->adhesionForce + Fn); // adhesionForce already included in normalForce (above)
		if (shearElastic.squaredNorm() > maxFs * maxFs) {
			phys->isSliding = true;
			noShearDamp     = true; // no damping is added in the shear direction, hence no need to account for shear damping dissipation
			Real ratio      = maxFs / shearElastic.norm();
			shearElastic *= ratio;
			phys->shearForce = shearElastic; /*store only elastic shear displacement*/
			us_elastic *= ratio;
			if (calcEnergy) {
				frictionDissipation += (us_total - prevUs_tot).dot(shearElastic);
			}                                     // calculate energy dissipation due to sliding behavior
		} else if (useDamping) {                      // add current contact damping if we do not slide and if damping is requested
			phys->shearViscous = cs * incidentVs; // get shear viscous component
			phys->shearForce   = shearElastic - phys->shearViscous;
		} else if (!useDamping) {
			phys->shearForce = shearElastic;
		} // update the shear force at the elastic value if no damping is present and if we passed MC
	}

	/************************/
	/* SHEAR ELASTIC ENERGY */
	/************************/

	// NOTE: shear elastic energy calculation must come after the MC criterion, otherwise displacements and forces are not updated
	if (calcEnergy) {
		shearEnergy
		        += (us_elastic - prevUs_el)
		                   .dot((shearElastic + prev_FsElastic)
		                        / 2.); // NOTE: no additional energy if we perform sliding since us_elastic and prevUs_el will hold the same value (in fact us_elastic is only keeping the elastic part). We work out the area of the trapezium.
	}

	/**************************************************/
	/* VISCOUS DAMPING (energy term, shear direction) */
	/**************************************************/

	if (useDamping) { // get normal viscous component (the shear one is calculated inside Mohr-Coulomb criterion, see above)
		if (calcEnergy) {
			if (!noShearDamp) { shearDampDissip += phys->shearViscous.dot(incidentVs * dt); }
		} // calc energy dissipation due to viscous linear damping
	}

	/****************/
	/* APPLY FORCES */
	/****************/

	if (!scene->isPeriodic)
		applyForceAtContactPoint(-phys->normalForce - phys->shearForce, scg->contactPoint, id1, de1->se3.position, id2, de2->se3.position);
	else { // in scg we do not wrap particles positions, hence "applyForceAtContactPoint" cannot be used
		Vector3r force = -phys->normalForce - phys->shearForce;
		scene->forces.addForce(id1, force);
		scene->forces.addForce(id2, -force);
		scene->forces.addTorque(id1, (scg->radius1 - 0.5 * scg->penetrationDepth) * scg->normal.cross(force));
		scene->forces.addTorque(id2, (scg->radius2 - 0.5 * scg->penetrationDepth) * scg->normal.cross(force));
	}

	/********************************************/
	/* MOMENT CONTACT LAW */
	/********************************************/
	if (includeMoment) {
		/*my new code */
		phys->kr = phys->kro * math::pow(uN, 0.5);
		/* my new code end */

		// *** Bending ***//
		// new code to compute relative particle rotation (similar to the way the shear is computed)
		// use scg function to compute relAngVel
		Vector3r relAngVel = scg->getRelAngVel(de1, de2, dt);
		//Vector3r relAngVel = (b2->state->angVel-b1->state->angVel);
		Vector3r relAngVelBend = relAngVel - scg->normal.dot(relAngVel) * scg->normal; // keep only the bending part
		Vector3r relRot        = relAngVelBend * dt;                                   // relative rotation due to rolling behaviour
		// incremental formulation for the bending moment (as for the shear part)
		Vector3r& momentBend = phys->momentBend;
		momentBend           = scg->rotate(momentBend);        // rotate moment vector (updated)
		momentBend           = momentBend - phys->kr * relRot; // add incremental rolling to the rolling vector
		// ----------------------------------------------------------------------------------------
		// *** Torsion ***//
		Vector3r relAngVelTwist = scg->normal.dot(relAngVel) * scg->normal;
		Vector3r relRotTwist    = relAngVelTwist * dt; // component of relative rotation along n
		// incremental formulation for the torsional moment
		Vector3r& momentTwist = phys->momentTwist;
		momentTwist           = scg->rotate(momentTwist); // rotate moment vector (updated)
		momentTwist           = momentTwist - phys->ktw * relRotTwist;

#if 0
	// code to compute the relative particle rotation
	if (includeMoment){
		Real rMean = (scg->radius1+scg->radius2)/2.;
		// sliding motion
		Vector3r duS1 = scg->radius1*(phys->prevNormal-scg->normal);
		Vector3r duS2 = scg->radius2*(scg->normal-phys->prevNormal);
		// rolling motion
		Vector3r duR1 = scg->radius1*dt*b1->state->angVel.cross(scg->normal);
		Vector3r duR2 = -scg->radius2*dt*b2->state->angVel.cross(scg->normal);
		// relative position of the old contact point with respect to the new one
		Vector3r relPosC1 = duS1+duR1;
		Vector3r relPosC2 = duS2+duR2;

		Vector3r duR = (relPosC1+relPosC2)/2.; // incremental displacement vector (same radius is temporarily assumed)

		// check wheter rolling will be present, if not do nothing
		Vector3r x=scg->normal.cross(duR);
		Vector3r normdThetaR(Vector3r::Zero()); // initialize
		if(x.squaredNorm()==0) { /* no rolling */ }
		else {
				Vector3r normdThetaR = x/x.norm(); // moment unit vector
				phys->dThetaR = duR.norm()/rMean*normdThetaR;} // incremental rolling

		// incremental formulation for the bending moment (as for the shear part)
		Vector3r& momentBend = phys->momentBend;
		momentBend = scg->rotate(momentBend); // rotate moment vector
		momentBend = momentBend+phys->kr*phys->dThetaR; // add incremental rolling to the rolling vector FIXME: is the sign correct?
#endif

		// check plasticity condition (only bending part for the moment)
		Real MomentMax    = phys->maxBendPl * phys->normalForce.norm();
		Real scalarMoment = phys->momentBend.norm();
		if (MomentMax > 0) {
			if (scalarMoment > MomentMax) {
				Real ratio = MomentMax / scalarMoment; // to fix the moment to its yielding value
				phys->momentBend *= ratio;
			}
		}
		// apply moments
		Vector3r moment = phys->momentTwist + phys->momentBend;
		scene->forces.addTorque(id1, -moment);
		scene->forces.addTorque(id2, moment);
	}
	return true;
	// update variables
	//phys->prevNormal = scg->normal;
}


} // namespace yade
