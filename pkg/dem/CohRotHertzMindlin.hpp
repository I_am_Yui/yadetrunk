// 2023 © Herry CHEN <qqing.chen@connect.polyu.hk>
//
/*
=== HIGH LEVEL OVERVIEW OF MINDLIN ===

Mindlin is a set of classes to include the Hertz-Mindlin formulation for the contact stiffnesses.
The DMT formulation is also considered (for adhesive particles, rigid and small bodies).

The cohesion is considered in the normal and shearing direction

*/

#pragma once

#include <core/Dispatching.hpp>
#include <pkg/common/ElastMat.hpp>
#include <pkg/common/MatchMaker.hpp>
#include <pkg/common/NormShearPhys.hpp>
#include <pkg/common/PeriodicEngines.hpp>
#include <pkg/dem/FrictPhys.hpp>
#include <pkg/dem/HertzMindlin.hpp>
#include <pkg/dem/ScGeom.hpp>

#include <lib/base/openmp-accu.hpp>
#include <boost/tuple/tuple.hpp>

namespace yade { // Cannot have #include directive inside.

/******************** CRMindlinMat **********************************/
class CohRotFricMat : public FrictMat {
public:
	virtual ~CohRotFricMat() {};
	
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(CohRotFricMat,FrictMat,"Material description extending :yref:`FrictMat` with rolling resistance, cohesion and stiffness reduction ",
		// Cohesion part
		((bool,isCohesive,true,,"Whether this body can form possibly cohesive interactions (if true and depending on other parameters such as :yref:`Ip2_CohFrictMat_CohFrictMat_CohFrictPhys.setCohesionNow`)."))
		((Real,normalCohesion,-1,,"Tensile strength, homogeneous to a pressure. If negative the normal force is purely elastic."))
		((Real,shearCohesion,-1,,"Shear strength, homogeneous to a pressure. If negative the shear force is purely elastic."))
        ((bool,fragile,true,,"does cohesion disappear when contact strength is exceeded?"))
		// bond part
        ((Real,pb_rmul_,1.0,,"bond-radius multiplier"))
        ((Real,pb_E_,0.0,,"Young's modulus for bond/cement  "))
        ((Real,pb_knks_ratio_,0.0,,"ratio of normal stiffness and shear stiffness"))
        ((Real,pb_ten_,0.0,,"bond normal, tensile strength "))
        ((Real,pb_shear_,-1.0,,"bond tangential, shear strength "))
        ((Real,pb_coh_,0.0,,"bond cohesion for shear strength"))
        ((Real,pb_fa_,0.0,," bond friction angle (in radians) for shear strength  "))
        
        // rolling resitance part
		((Real,alphaKr,1.0,,"Dimensionless rolling stiffness."))
		((Real,etaRoll,0.5,,"Dimensionless rolling (aka 'bending') strength. If negative, rolling moment will be elastic."))
        ((Real,alphaKtw,1.0,,"Dimensionless twist stiffness."))
		((Real,etaTwist,0.5,,"Dimensionless twisting strength. If negative, twist moment will be elastic."))
		// reduction part
		((Real,alphag,0.01,,"Residual contact stiffness coefficient "))
		((Real,lambda_r,2.0e-3,,"Residual contact stiffness coefficient lambda_r"))
		((Real,pow_c,1.0,,"The pow control the degradation rate of contact stiffness"))
		,
		createIndex();
		);
	// clang-format on
	/// Indexable
	REGISTER_CLASS_INDEX(CohRotFricMat, FrictMat);
};

REGISTER_SERIALIZABLE(CohRotFricMat);

/******************** CohRotMindlinPhys *********************************/
class CohRotMindlinPhys : public MindlinPhys {
public:
	virtual ~CohRotMindlinPhys() = default;
	void SetBreakingState() {cohesionBroken = true; normalAdhesion = 0; shearAdhesion = 0;};

	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(CohRotMindlinPhys,MindlinPhys,"Representation of an interaction of the Hertz-Mindlin type.",
			/* cohesion part*/
			((bool,cohesionDisablesFriction,false,,"is shear strength the sum of friction and adhesion or only adhesion?"))
			((bool,cohesionBroken,true,,"is cohesion active? Set to false at the creation of a cohesive contact, and set to true when a fragile contact is broken"))
			((bool,fragile,true,,"do cohesion disappear when contact strength is exceeded?"))
			((Real,normalAdhesion,0,,"tensile strength"))
			((Real,shearAdhesion,0,,"cohesive part of the shear strength (a frictional term might be added depending on :yref:`CohFrictPhys::cohesionDisablesFriction`)"))
			((Real,unp,0,,"plastic normal displacement, only used for tensile behaviour and if :yref:`CohFrictPhys::fragile` =false."))
			((Real,unpMax,0,,"maximum value of plastic normal displacement (counted positively), after that the interaction breaks even if :yref:`CohFrictPhys::fragile` =false. A negative value (i.e. -1) means no maximum."))
			((bool,initCohesion,false,,"Initialize the cohesive behaviour with current state as equilibrium state (same as :yref:`Ip2_CohFrictMat_CohFrictMat_CohFrictPhys::setCohesionNow` but acting on only one interaction)"))
			/* bond part, using the _ in the end of variable to show it belong to bond part*/
			((bool,pb_state_,false,," true for bonding, flase for breaking"))
			((Real,kn_,0.0,," bond normal stiffness"))
			((Real,ks_,0.0,," bond shear stiffness"))
		 	((Real,pb_ten_,0.0,," bond tensile  [normal]"))
			((Real,pb_coh_,0.0,," bond cohesion [shear]"))
			((Real,pb_fa_,0.0,, " bond friction angle [shear]"))
			((Real,rgap_,0.0,," reference gap"))
			((Vector3r,pb_Fn_,Vector3r::Zero(),," bond normal force"))
			((Vector3r,pb_Fs_,Vector3r::Zero(),," bond shear force"))
			((Vector3r,pb_Mb_,Vector3r::Zero(),," bond bending or rolling moment"))
			((Vector3r,pb_Mt_,Vector3r::Zero(),," bond twist moment"))
			((Real,R_,0.0,,"radius of the bond cross section"))
			//((Real,A_,0.0,,"area of the bond cross section"))
       		//((Real,I_,0.0,,"moment of inertia of the bond cross section"))
       		//((Real,J_,0.0,,"polar moment of inertia of the bond cross section")) 
			/* rolling part*/
			((Real,kro,0.0,," Rolling or rotation stiffness"))
			((Real,kto,0.0,," Twisting stiffness"))
			/* new parameter*/
			((Real,rho,1.0,,"a boundary ratio "))
			((Real,phi,0.0,,"a boundary distance "))
			((Real,alphag,0.0,,"Residual contact stiffness coefficient alphag"))
			((Real,lambda_r,0.0,,"Residual contact stiffness coefficient lambda_r"))
			((Real,pow_c,1.0,,"pow coefficient lambda_r"))
			((Real,ratio_knks,0.0,,"keep constant kn / ks"))
			((bool,initReduction,false,,"kbool to identify if the contact stiffness can reduction"))
			((Real,accumS,0.0,,"Total tangential displacement's magnitual"))
			((Real,accumS0,1.0,,"Total tangential displacement's magnitual"))
			((Real,kso_o,0.0,,"the tangential  stiffness, no constant"))
			((Real,kno_o,0.0,,"the normal stiffness , not constant"))
			((Real,kso_r,0.0,,"the tangential reduction stiffness"))
			((Real,kno_r,0.0,,"the normal reduction stiffness"))
			((Real,reductionCoefficient,0.0,,"the reduction coefficient "))
			,
			createIndex());
	// clang-format on
	REGISTER_CLASS_INDEX(CohRotMindlinPhys, MindlinPhys);
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(CohRotMindlinPhys);


/******************** Ip2_CohRotFricMat_CohRotFricMat_CohRotMindlinPhys *******/
class Ip2_CohRotFricMat_CohRotFricMat_CohRotMindlinPhys : public IPhysFunctor {
public:
	void go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction) override;
	FUNCTOR2D(CohRotFricMat, CohRotFricMat);

	//Real InitalShearDisplacemet = 0.0;//can we do like this?
	Real InitalShearDisplacemet ;
	int  ShearDis_DefinitionIteration;// we need this two variable?
    // cohesion setting
    int cohesionDefinitionIteration;
	// parreal setting
    int pbondDefinitionIteration;

	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(
			Ip2_CohRotFricMat_CohRotFricMat_CohRotMindlinPhys,IPhysFunctor, 
				R"""(Calculate  the reduction stiffness contact)""",
			((bool,setStiffReduction,false,,"If true, assign all cotact (include the new contact) 's stiffness to be reductionable"))
            // Generates cohesive interactions in the contact law 
            ((bool,setPBondNow,false,,"If true, assign cohesion to all existing contacts in current time-step. The flag is turned false automatically, so that assignment is done in the current timestep only."))
		    // Generates cohesive interactions in the contact law 
		    ((bool,setCohesionNow,false,,"If true, assign cohesion to all existing contacts in current time-step. The flag is turned false automatically, so that assignment is done in the current timestep only."))
		    ((bool,setCohesionOnNewContacts,false,,"If true, assign cohesion at all new contacts. If false, only existing contacts can be cohesive (also see :yref:`Ip2_CohFrictMat_CohFrictMat_CohFrictPhys::setCohesionNow`), and new contacts are only frictional."))	
		    ((shared_ptr<MatchMaker>,normalCohesion,,,"Instance of :yref:`MatchMaker` determining tensile strength"))
		    ((shared_ptr<MatchMaker>,shearCohesion,,,"Instance of :yref:`MatchMaker` determining cohesive part of the shear strength (a frictional term might be added depending on :yref:`CohFrictPhys::cohesionDisablesFriction`)"))
		    // The original parameter of the hertz model
			((Real,gamma,0.0,,"Surface energy parameter [J/m^2] per each unit contact surface, to derive DMT formulation from HM"))
			((Real,eta,0.0,,"Coefficient to determine the plastic bending moment"))
			((Real,krot,0.0,,"Rotational stiffness for moment contact law"))
			((Real,ktwist,0.0,,"Torsional stiffness for moment contact law"))
			((shared_ptr<MatchMaker>,en,,,"Normal coefficient of restitution $e_n$."))
			((shared_ptr<MatchMaker>,es,,,"Shear coefficient of restitution $e_s$."))
			((shared_ptr<MatchMaker>,betan,,,"Normal viscous damping ratio $\\beta_n$."))
			((shared_ptr<MatchMaker>,betas,,,"Shear viscous damping ratio $\\beta_s$."))
			((shared_ptr<MatchMaker>,frictAngle,,,"Instance of :yref:`MatchMaker` determining how to compute the friction angle of an interaction. If ``None``, minimum value is used."))
			,
			InitalShearDisplacemet = 0.0;
			ShearDis_DefinitionIteration = -1;
            cohesionDefinitionIteration = -1;
            pbondDefinitionIteration = -1;
	);
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Ip2_CohRotFricMat_CohRotFricMat_CohRotMindlinPhys);
  
/******************** Law2_ScGeom_CohRotMindlinPhys_CRMindlin *********/
class Law2_ScGeom_CohRotMindlinPhys_CRMindlin : public LawFunctor {
public:
	bool go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I) override;
	Real normElastEnergy();
	Real adhesionEnergy();

	Real getfrictionDissipation() const;
	Real getshearEnergy() const;
	Real getnormDampDissip() const;
	Real getshearDampDissip() const;
	Real contactsAdhesive();
	Real ratioSlidingContacts();

	FUNCTOR2D(ScGeom, CohRotMindlinPhys);
	// clang-format off
		YADE_CLASS_BASE_DOC_ATTRS_DEPREC_INIT_CTOR_PY(Law2_ScGeom_CohRotMindlinPhys_CRMindlin,LawFunctor,"Constitutive law for the Hertz-Mindlin formulation. It includes non linear elasticity in the normal direction as predicted by Hertz for two non-conforming elastic contact bodies. In the shear direction, instead, it reseambles the simplified case without slip discussed in Mindlin's paper, where a linear relationship between shear force and tangential displacement is provided. Finally, the Mohr-Coulomb criterion is employed to established the maximum friction force which can be developed at the contact. Moreover, it is also possible to include the effect of linear viscous damping through the definition of the parameters $\\beta_{n}$ and $\\beta_{s}$.",
			/* reduction control*/
			((bool,includeReduction,false,," bool to include the stiffness reduction function "))
			((bool,normalReduction,true,," bool to allow normal  stiffness reduction function "))
			((bool,shearReduction,true,," bool to allow shear  the stiffness reduction function "))
			((bool,rollingReduction,false,," bool to allow rolling stiffness degradation "))
		    ((bool,isHyperbolic,false,,"Whether the degradation is using a hyperbolic method . Default is use my boudary surface reduction method. "))
			/* cohesoin control*/
			/* original hertz model*/
			((bool,includeAdhesion,false,,"bool to include the adhesion force following the DMT formulation. If true, also the normal elastic energy takes into account the adhesion effect."))
			((bool,calcEnergy,false,,"bool to calculate energy terms (shear potential energy, dissipation of energy due to friction and dissipation of energy due to normal and tangential damping)"))
			((bool,includeMoment,false,,"bool to consider rolling resistance (if :yref:`Ip2_CohRotFricMat_CohRotFricMat_CohRotMindlinPhys::eta` is 0.0, no plastic condition is applied.)"))
			((bool,neverErase,false,,"Keep interactions even if particles go away from each other (only in case another constitutive law is in the scene, e.g. :yref:`Law2_ScGeom_CapillaryPhys_Capillarity`)"))
			((bool,nothing,false,,"dummy attribute for declaring preventGranularRatcheting deprecated"))
			//((bool,LinDamp,true,,"bool to activate linear viscous damping (if false, en and es have to be defined in place of betan and betas)"))

			((OpenMPAccumulator<Real>,frictionDissipation,,Attr::noSave,"Energy dissipation due to sliding"))
			((OpenMPAccumulator<Real>,shearEnergy,,Attr::noSave,"Shear elastic potential energy"))
			((OpenMPAccumulator<Real>,normDampDissip,,Attr::noSave,"Energy dissipated by normal damping"))
			((OpenMPAccumulator<Real>,shearDampDissip,,Attr::noSave,"Energy dissipated by tangential damping"))
			, /* deprec */			
			((preventGranularRatcheting, nothing,"this value is no longer used, don't define it."))
			, /* init */
			, /* ctor */
			, /* py */
			.def("contactsAdhesive",&Law2_ScGeom_CohRotMindlinPhys_CRMindlin::contactsAdhesive,"Compute total number of adhesive contacts.")
			.def("ratioSlidingContacts",&Law2_ScGeom_CohRotMindlinPhys_CRMindlin::ratioSlidingContacts,"Return the ratio between the number of contacts sliding to the total number at a given time.")
			.def("normElastEnergy",&Law2_ScGeom_CohRotMindlinPhys_CRMindlin::normElastEnergy,"Compute normal elastic potential energy. It handles the DMT formulation if :yref:`Law2_ScGeom_CohRotMindlinPhys_CRMindlin::includeAdhesion` is set to true.")
// 			.add_property("preventGranularRatcheting",&Law2_ScGeom_CohRotMindlinPhys_CRMindlin::deprecAttr,&Law2_ScGeom_CohRotMindlinPhys_CRMindlin::deprecAttr,"Warn that this is no longer used")
	);
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_CohRotMindlinPhys_CRMindlin);


} // namespace yade
