// 2023 © Herry CHEN <qqing.chen@connect.polyu.hk>
//
/*
=== HIGH LEVEL OVERVIEW OF MINDLIN ===

Mindlin is a set of classes to include the Hertz-Mindlin formulation for the contact stiffnesses.
The DMT formulation is also considered (for adhesive particles, rigid and small bodies).

*/

#pragma once

#include <core/Dispatching.hpp>
#include <pkg/common/ElastMat.hpp>
#include <pkg/common/MatchMaker.hpp>
#include <pkg/common/NormShearPhys.hpp>
#include <pkg/common/PeriodicEngines.hpp>
#include <pkg/dem/FrictPhys.hpp>
#include <pkg/dem/HertzMindlin.hpp>
#include <pkg/dem/ScGeom.hpp>

#include <lib/base/openmp-accu.hpp>
#include <boost/tuple/tuple.hpp>

namespace yade { // Cannot have #include directive inside.

/******************** myMindlinMat **********************************/
class myFrictMat : public FrictMat {
public:
	virtual ~myFrictMat() {};
	
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(myFrictMat,FrictMat,"Material description extending :yref:`FrictMat` with  stiffness reduction ",
		// rolling resitance part
		((bool,isCohesive,true,,"Whether this body can form possibly cohesive interactions (if true and depending on other parameters such as :yref:`Ip2_CohFrictMat_CohFrictMat_CohFrictPhys.setCohesionNow`)."))
		((Real,alphaKr,0.1,,"Dimensionless rolling stiffness."))
		((Real,etaRoll,0.5,,"Dimensionless rolling (aka 'bending') strength. If negative, rolling moment will be elastic."))
		((Real,normalCohesion,-1,,"Tensile strength, homogeneous to a pressure. If negative the normal force is purely elastic."))
		((Real,shearCohesion,-1,,"Shear strength, homogeneous to a pressure. If negative the shear force is purely elastic."))
		// reduction part
		((Real,alpha,0.01,,"Residual contact stiffness coefficient "))
		((Real,lambda_r,2.0e-3,,"Residual contact stiffness coefficient lambda_r"))
		((Real,pow_c,1.0,,"the coeffiecient of the pow"))
		,
		createIndex();
		);
	// clang-format on
	/// Indexable
	REGISTER_CLASS_INDEX(myFrictMat, FrictMat);
};

REGISTER_SERIALIZABLE(myFrictMat);

/******************** myMindlinPhys *********************************/
class myMindlinPhys : public MindlinPhys {
public:
	virtual ~myMindlinPhys() = default;

	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(myMindlinPhys,MindlinPhys,"Representation of an interaction of the Hertz-Mindlin type.",
			/* rolling part*/
			((Real,kro,0.0,,"Constant value in the formulation of the rolling stiffness"))
			/* new parameter*/
			((Real,rho,1.0,,"a boundary ratio "))
			((Real,phi,0.0,,"a boundary distance "))
			((Real,alpha,0.0,,"Residual contact stiffness coefficient alpha"))
			((Real,lambda_r,0.0,,"Residual contact stiffness coefficient lambda_r"))
			((Real,pow_c,1.0,,"pow coefficient lambda_r"))
			((Real,ratio_knks,0.0,,"keep constant kn / ks"))
			((bool,initReduction,false,,"kbool to identify if the contact stiffness can reduction"))
			((Real,accumS,0.0,,"Total tangential displacement's magnitual"))
			((Real,accumS0,1.0,,"Total tangential displacement's magnitual"))
			((Real,kso_o,0.0,,"the tangential  stiffness, no constant"))
			((Real,kno_o,0.0,,"the normal stiffness , not constant"))
			((Real,kso_r,0.0,,"the tangential reduction stiffness"))
			((Real,kno_r,0.0,,"the normal reduction stiffness"))
			((Real,reductionCoefficient,0.0,,"the reduction coefficient "))
			,
			createIndex());
	// clang-format on
	REGISTER_CLASS_INDEX(myMindlinPhys, MindlinPhys);
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(myMindlinPhys);


/******************** Ip2_myFrictMat_myFrictMat_myMindlinPhys *******/
class Ip2_myFrictMat_myFrictMat_myMindlinPhys : public IPhysFunctor {
public:
	void go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction) override;
	FUNCTOR2D(myFrictMat, myFrictMat);

	//Real InitalShearDisplacemet = 0.0;//can we do like this?
	Real InitalShearDisplacemet ;
	int  ShearDis_DefinitionIteration;

	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(
			Ip2_myFrictMat_myFrictMat_myMindlinPhys,IPhysFunctor, 
				R"""(Calculate  the reduction stiffness contact)""",
			((bool,setStiffReduction,false,,"If true, assign all cotact (include the new contact) 's stiffness to be reductionable"))
			((Real,gamma,0.0,,"Surface energy parameter [J/m^2] per each unit contact surface, to derive DMT formulation from HM"))
			((Real,eta,0.0,,"Coefficient to determine the plastic bending moment"))
			((Real,krot,0.0,,"Rotational stiffness for moment contact law"))
			((Real,ktwist,0.0,,"Torsional stiffness for moment contact law"))
			((shared_ptr<MatchMaker>,en,,,"Normal coefficient of restitution $e_n$."))
			((shared_ptr<MatchMaker>,es,,,"Shear coefficient of restitution $e_s$."))
			((shared_ptr<MatchMaker>,betan,,,"Normal viscous damping ratio $\\beta_n$."))
			((shared_ptr<MatchMaker>,betas,,,"Shear viscous damping ratio $\\beta_s$."))
			((shared_ptr<MatchMaker>,frictAngle,,,"Instance of :yref:`MatchMaker` determining how to compute the friction angle of an interaction. If ``None``, minimum value is used."))
			,
			InitalShearDisplacemet = 0.0;
			ShearDis_DefinitionIteration = -1;
	);
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Ip2_myFrictMat_myFrictMat_myMindlinPhys);
  
/******************** Law2_ScGeom_myMindlinPhys_myMindlin *********/
class Law2_ScGeom_myMindlinPhys_myMindlin : public LawFunctor {
public:
	bool go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I) override;
	Real normElastEnergy();
	Real adhesionEnergy();

	Real getfrictionDissipation() const;
	Real getshearEnergy() const;
	Real getnormDampDissip() const;
	Real getshearDampDissip() const;
	Real contactsAdhesive();
	Real ratioSlidingContacts();

	FUNCTOR2D(ScGeom, myMindlinPhys);
	// clang-format off
		YADE_CLASS_BASE_DOC_ATTRS_DEPREC_INIT_CTOR_PY(Law2_ScGeom_myMindlinPhys_myMindlin,LawFunctor,"Constitutive law for the Hertz-Mindlin formulation. It includes non linear elasticity in the normal direction as predicted by Hertz for two non-conforming elastic contact bodies. In the shear direction, instead, it reseambles the simplified case without slip discussed in Mindlin's paper, where a linear relationship between shear force and tangential displacement is provided. Finally, the Mohr-Coulomb criterion is employed to established the maximum friction force which can be developed at the contact. Moreover, it is also possible to include the effect of linear viscous damping through the definition of the parameters $\\beta_{n}$ and $\\beta_{s}$.",
			((bool,includeReduction,false,," bool to include the stiffness reduction function "))
			((bool,normalReduction,true,," bool to allow normal  stiffness reduction function "))
			((bool,shearReduction,true,," bool to allow shear  the stiffness reduction function "))
		    ((bool,isHyperbolic,false,,"Whether the degradation is using a hyperbolic method . Default is use my boudary surface reduction method. "))

			
			((bool,includeAdhesion,false,,"bool to include the adhesion force following the DMT formulation. If true, also the normal elastic energy takes into account the adhesion effect."))
			((bool,calcEnergy,false,,"bool to calculate energy terms (shear potential energy, dissipation of energy due to friction and dissipation of energy due to normal and tangential damping)"))
			((bool,includeMoment,false,,"bool to consider rolling resistance (if :yref:`Ip2_myFrictMat_myFrictMat_myMindlinPhys::eta` is 0.0, no plastic condition is applied.)"))
			((bool,neverErase,false,,"Keep interactions even if particles go away from each other (only in case another constitutive law is in the scene, e.g. :yref:`Law2_ScGeom_CapillaryPhys_Capillarity`)"))
			((bool,nothing,false,,"dummy attribute for declaring preventGranularRatcheting deprecated"))
			//((bool,LinDamp,true,,"bool to activate linear viscous damping (if false, en and es have to be defined in place of betan and betas)"))

			((OpenMPAccumulator<Real>,frictionDissipation,,Attr::noSave,"Energy dissipation due to sliding"))
			((OpenMPAccumulator<Real>,shearEnergy,,Attr::noSave,"Shear elastic potential energy"))
			((OpenMPAccumulator<Real>,normDampDissip,,Attr::noSave,"Energy dissipated by normal damping"))
			((OpenMPAccumulator<Real>,shearDampDissip,,Attr::noSave,"Energy dissipated by tangential damping"))
			, /* deprec */			
			((preventGranularRatcheting, nothing,"this value is no longer used, don't define it."))
			, /* init */
			, /* ctor */
			, /* py */
			.def("contactsAdhesive",&Law2_ScGeom_myMindlinPhys_myMindlin::contactsAdhesive,"Compute total number of adhesive contacts.")
			.def("ratioSlidingContacts",&Law2_ScGeom_myMindlinPhys_myMindlin::ratioSlidingContacts,"Return the ratio between the number of contacts sliding to the total number at a given time.")
			.def("normElastEnergy",&Law2_ScGeom_myMindlinPhys_myMindlin::normElastEnergy,"Compute normal elastic potential energy. It handles the DMT formulation if :yref:`Law2_ScGeom_myMindlinPhys_myMindlin::includeAdhesion` is set to true.")
// 			.add_property("preventGranularRatcheting",&Law2_ScGeom_myMindlinPhys_myMindlin::deprecAttr,&Law2_ScGeom_myMindlinPhys_myMindlin::deprecAttr,"Warn that this is no longer used")
	);
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_myMindlinPhys_myMindlin);


} // namespace yade
